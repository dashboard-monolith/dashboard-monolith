'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {

    checkPassword = password => bcrypt.compareSync(password, this.password);

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.email
      };
      const rahasia = 'dashboard-monolith-binar';
      // Membuat token dari data-data diatas
      const token = jwt.sign(payload, rahasia, { expiresIn: '1h' });
      return token;
    };

    static authenticate = async (email, password) => {
      try {
        const user = await this.findOne({ where: { email: email } });
        if (!user) return Promise.reject("User not found!");
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Wrong password");
        return Promise.resolve(user);
      }
      catch (err) {
        return Promise.reject(err);
      }
      /* Akhir dari semua yang berhubungan dengan login */
    };
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsTo(models.Article, { foreignKey: 'id', as: 'artouser' });
    }
  };
  User.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};