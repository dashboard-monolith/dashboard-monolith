const express = require('express');
const router = express.Router();

const userCtrl = require('../controllers/userController');
const homeCtrl = require('../controllers/homeController');
const arCtrl = require("../controllers/articleController");


/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.get('/', arCtrl.findAllArticle, homeCtrl.renderMain);
router.get('/article/:id', arCtrl.findOneArticle, homeCtrl.renderArticle);

router.get('/login', function(req, res, next) {
  res.render('login');
});

router.get('/register', function(req, res, next) {
  res.render('register');
});

router.post('/login', userCtrl.login);

router.post('/register', userCtrl.register);

router.get('/logout', userCtrl.logout);

router.post('/search', arCtrl.findByQuery, homeCtrl.search);

module.exports = router;
