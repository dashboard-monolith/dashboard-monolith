const express = require('express');
const router = express.Router();
const auth = require('../middlewares/auth');

const ctrl = require("../controllers/articleController");
const usCtrl = require("../controllers/userController");
const dbCtrl = require("../controllers/dashboardController");


/* GET home page. */
router.use(auth);

router.get('/', usCtrl.userCount, ctrl.articleCount, dbCtrl.main);

// router.get('/articles', function (req, res, next) {
//     res.render('dashboard/articles');
// });

router.get('/create-article', function (req, res, next) {
    res.render('dashboard/create-article');
});

router.post('/create', ctrl.createArticle);
router.post('/search', ctrl.findByQuery, dbCtrl.search);
router.get('/articles', ctrl.findAllArticle, ctrl.renderAllArticles);
router.get('/article/:id', ctrl.findOneArticle, ctrl.renderOneArticle);

router.get('/article/edit/:id', ctrl.findOneArticle, ctrl.renderEdit);
router.post('/article/update/:id', ctrl.updateArticle);
router.get('/article/delete/:id', ctrl.deleteArticle);

module.exports = router;
