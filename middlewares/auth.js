const passport = require('../lib/passport');
const __export = (req, res, next) => {
    const userToken = req.cookies.jwt;
    if (userToken) {
        req.headers.authorization = userToken;
        passport.authenticate('jwt', {
            session: false
        });
        next();
    } else {
        return res.redirect('/login');
    }
};

module.exports = __export;