const bcrypt = require("bcrypt");
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const date = new Date().toISOString();
    const users = await queryInterface.bulkInsert('Users', [{
      name: 'Estelle',
      email: 'estelle@email.com',
      password: bcrypt.hashSync('123456', 10),
      createdAt: date,
      updatedAt: date
    }], { returning: true });
    await queryInterface.bulkInsert('Articles', [{
      title: 'John Doe',
      body: 'Did you know who is the most popular man in programming? I do not know',
      approved: false,
      writer: users[0].id,
      createdAt: date,
      updatedAt: date
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
