const bcrypt = require("bcrypt");
'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.addConstraint('Articles', {
      fields: ['writer'],
      type: 'FOREIGN KEY',
      name: 'FK_user_id ', // useful if using queryInterface.removeConstraint
      references: {
        table: 'Users',
        field: 'id',
      },
      onDelete: 'no action',
      onUpdate: 'no action',
    });
    const date = new Date().toISOString();
    const users = await queryInterface.bulkInsert('Users', [{
      name: 'Admin',
      email: 'admin@email.com',
      password: bcrypt.hashSync('123456', 10),
      createdAt: date,
      updatedAt: date
    }], { returning:true });
    await queryInterface.bulkInsert('Articles', [{
      title: 'John Doe',
      body: 'Did you know who is the most popular man in programming? I do not know',
      approved: false,
      writer: users[0].id,
      createdAt: date,
      updatedAt: date
    }], {});
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Articles');
    await queryInterface.dropTable('Users');
  }
};