const { Article, User } = require("../models");
const visitCounter = require('express-visit-counter').Loader;
const jwt = require('jsonwebtoken');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = {
  createArticle: async (req, res) => {
    try {
      const { title, body } = req.body;
      const decoded = jwt.verify(req.headers.authorization, 'dashboard-monolith-binar');
      const writerId = decoded.id;

      await Article.create({
        title: title,
        body: body,
        approved: false,
        writer: writerId
      });
      res.redirect("/dashboard/articles");
    } catch (error) {
      console.log(error);
      res.send(error);
    }
  },
  findAllArticle: async (req, res, next) => {
    try {
      const articles = await Article.findAll({
        include: [
          {
            model: User,
            as: 'usertoar'
          }
        ]
      });
      res.locals.data = articles;
      next();
    } catch (error) {
      res.send(error);
    }
  },
  findOneArticle: async (req, res, next) => {
    try {
      const article = await Article.findOne({ where: { id: req.params.id } });
      res.locals.data = article;
      next();
    } catch (error) {
      res.send(error);
    }
  },
  findByQuery: async (req, res, next) => {
    const title = req.body.title;
    const article = await Article.findAll({
      where: {
        title: { [Op.iLike]: `%${title}%` }
      }, include: [
        {
          model: User,
          as: 'usertoar'
        }
      ]
    });
    res.locals.data = article;
    next();
  },
  articleCount: async (req, res, next) => {
    try {
      const articleCount = await Article.findAndCountAll({ attributes: ['id'] });
      res.locals.articleCount = articleCount.count;
      next();
    } catch (error) {
      res.send(error);
    }
  },
  updateArticle: async (req, res) => {
    const { title, body, approved } = req.body;
    let approvednc
    if (approved !== undefined) {
      approvednc = true;
    } else {
      approvednc = false;
    }
    try {
      await Article.update(
        {
          title: title,
          body: body,
          approved: approvednc
        },
        { where: { id: req.params.id } }
      );
      res.redirect("/dashboard/articles");
    } catch (error) {
      res.send(error);
    }
  },
  deleteArticle: async (req, res) => {
    try {
      await Article.destroy({ where: { id: req.params.id } });
      res.redirect("/dashboard/articles");
    } catch (error) {
      res.send(error);
    }
  },
  renderAllArticles: async (req, res) => {
    const articles = res.locals.data;
    let viewCounts = [];
    for (let index = 0; index < articles.length; index++) {
      const element = articles[index];
      let visitorCount = await visitCounter.getCount(`/article/${element.id}`);
      viewCounts[index] = {
        viewCount: visitorCount
      }
    }
    res.render("dashboard/articles", { articles, viewCounts });
  },
  renderOneArticle: (req, res) => {
    const article = res.locals.data;
    res.render("dashboard/article", { article });
  },
  renderEdit: (req, res) => {
    const article = res.locals.data;
    res.render("dashboard/edit-article", { article });
  },
};
