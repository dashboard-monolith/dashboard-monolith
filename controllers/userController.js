const { User } = require("../models");
const bcrypt = require("bcrypt");


const __export = {
  login: (req, res) => {
    const { email, password } = req.body;
    if (email === undefined || password === undefined)
      res.json({ err: "Fill the blank form" });
    User.authenticate(email, password)
      .then((user) => {
        const { id, username } = user;
        const token = user.generateToken();
        res.setHeader("Authorization", token);
        res.cookie("jwt", token, {
          httpOnly: true,
          secure: true,
          maxAge: 3600000,
        });
        res.redirect("/dashboard");
      })
      .catch((err) => res.json({ err: err }));
  },
  logout: (req, res) => {
    res.cookie("jwt", {}, {
      httpOnly: true,
      secure: true,
      maxAge: -1,
    });
    res.redirect("/dashboard");
  },
  register: (req, res) => {
    const { name, email, password } = req.body;
    const hash = bcrypt.hashSync(password, 10);

    User.create({
      name: name,
      email: email,
      password: hash,
    })
      .then(() => {
        res.redirect("/login");
      })
      .catch((err) => res.json({ err: err }));
  },
  userCount: async (req, res, next) => {
    try {
      const userCount = await User.findAndCountAll({ attributes: ['id'] });
      res.locals.userCount = userCount.count;
      next();
    } catch (error) {
      res.send(error);
    }
  }
};

module.exports = __export;
