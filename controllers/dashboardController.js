const visitCounter = require('express-visit-counter').Loader;

const __export = {
    main: async (req, res, next) => {
        const userCount = res.locals.userCount;
        const articleCount = res.locals.articleCount;
        let allVisitor = await visitCounter.getCount();
        res.render('dashboard', { userCount, articleCount, allVisitor });
    },
    search: async(req, res) => {
        const preArticles = res.locals.data;
        let articles = [];
        let viewCounts = [];
        for (let index = 0; index < preArticles.length; index++) {
            const element = preArticles[index];
            let visitorCount = await visitCounter.getCount(`/article/${element.id}`);
            viewCounts[index] = {
                viewCount: visitorCount
            }
            articles.push(element);
        }
        res.render("dashboard/articles", { articles, viewCounts });
    }
}

module.exports = __export;