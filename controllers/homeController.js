const __export = {
    renderMain: (req, res) => {
        const preArticles = res.locals.data;
        let articles = [];
        for (let index = 0; index < preArticles.length; index++) {
            const element = preArticles[index];
            if (element.approved) {
                articles.push(element);
            }
        }
        res.render("index", { articles });
    },
    renderArticle: (req, res) => {
        const article = res.locals.data;
        if(article.approved) res.render("single-article", { article });
        else {
            const message = "Not Found";
            const error = {};
            error.status = 404;
            error.stack = 'Mungkin artikelnya belum diapprove, coba tanya adminnya aja bang';
            res.status(404).render('error', {message, error})
        }
    },
    search: (req, res) => {
        const preArticles = res.locals.data;
        let articles = [];
        for (let index = 0; index < preArticles.length; index++) {
            const element = preArticles[index];
            if (element.approved) {
                articles.push(element);
            }
        }
        res.render("index", { articles });
    }
}

module.exports = __export;